#include <cstdint>
#include <iostream>
#include <filesystem>
#include <memory>
#include <thread>

#include "logger.hpp"
#include "loggerTxt.hpp"
#include "simplegfx_helper.hpp"
#include "video.hpp"

using namespace std::literals;

int main(int argc, char **argv) {
    auto LOGGER = std::make_shared<SimpleGFX::logger>(SimpleGFX::loggingLevel::debug);
    
    if(argc == 1) {
        *LOGGER << SimpleGFX::loggingLevel::fatal << "you need to give a path to a video file";
        return 1;
    }
    
    if (argc > 3) {
        *LOGGER << SimpleGFX::loggingLevel::fatal << "more than one parameter given! This program only takes one.";
    }
    
    uint64_t benchmarkFrameAmount = 10000;
    if(argc == 3){
        benchmarkFrameAmount = static_cast<uint64_t>(std::stoi(argv[2]));
    }
    
    auto videoPath = std::filesystem::canonical(std::filesystem::path{argv[1]});
    *LOGGER << SimpleGFX::loggingLevel::normal << "Benchmarking the following file:" << videoPath;
    *LOGGER << SimpleGFX::loggingLevel::normal << "with " << benchmarkFrameAmount << " frames";
    
    std::unique_ptr<libtrainsim::Video::videoReader> decoder;
    try{
        decoder = std::make_unique<libtrainsim::Video::videoReader>(videoPath, LOGGER, 0, benchmarkFrameAmount * 2);
    }catch(...){
        *LOGGER << SimpleGFX::loggingLevel::fatal << "Cannot create video decoder.";
        LOGGER->logCurrrentException();
        return 1;
    }
    
    *LOGGER << SimpleGFX::loggingLevel::normal << "Running the benchmark...";
    bool normalExit = true;
    for(uint64_t frame = 0; frame <= benchmarkFrameAmount; frame++){
        try{
            decoder->requestFrame(frame);
        }catch(...){
            *LOGGER << SimpleGFX::loggingLevel::fatal << "Error while requesting frame:" << frame;
            LOGGER->logCurrrentException();
            return 1;
        }
        if(decoder->reachedEndOfFile()){
            *LOGGER <<SimpleGFX::loggingLevel::error << "reached EOF or got an error at frame:" << frame;
            normalExit = false;
            break;
        }
        std::this_thread::sleep_for(1ns);
    }
    
    if(normalExit){
        while(decoder->getFrameNumber() < benchmarkFrameAmount){
            std::this_thread::sleep_for(1ms);
        }
    }
    
    auto rendertimesOpt = decoder->getNewRendertimes();
    if(!rendertimesOpt.has_value()){
        *LOGGER << SimpleGFX::loggingLevel::normal << "somehow no rendertimes were returned!";
        return 1;
    }
    auto rendertimes = rendertimesOpt.value();
    auto rentertimeSum = sakurajin::unit_system::time_si{0.0};
    for(const auto& currTime:rendertimes){
        rentertimeSum += currTime;
    }
    
    *LOGGER << SimpleGFX::loggingLevel::normal << "Took " << rentertimeSum << " to decode " << benchmarkFrameAmount << " frames.";
    
    return 0;
}
